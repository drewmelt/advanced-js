class Employee {
	constructor(salary, name, age) {
		this._salary = salary
		this._name = name
		this._age = age
	}

	get name() {
		return this._name
	}

	set name(value) {
		this._name = value
	}

	get age() {
		return this._age
	}

	set age(value) {
		this._age = value
	}

	get salary() {
		return this._salary
	}

	set salary(value) {
		this._salary = value
	}
}

class Programmer extends Employee {
	constructor(salary, name, age, lang) {
		super(salary, name, age)
		this.lang = lang
	}

	get salary() {
		return (this._salary *= 3)
	}

	info() {
		console.log(
			`name: ${this.name}, salary: ${this.salary}, age: ${this.lang}, lang:[${this.lang}]`
		)
	}
}

const TomasProgrammer = new Programmer(500, 'Tomas', 45, [
	'PostSQL',
	'MySQl',
	'MongoDB',
])
const YaniProgrammer = new Programmer(3500, 'Yani', 34, [
	'Spring',
	'Java',
	'Golang',
	'NodeJS',
])
const DenProgrammer = new Programmer(1500, 'Den', 12, [
	'C',
	'C++',
	'MASM32',
	'NASM32',
	'FASM32',
])

TomasProgrammer.info()
YaniProgrammer.info()
DenProgrammer.info()
