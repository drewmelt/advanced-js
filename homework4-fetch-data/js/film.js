import { CreateElement } from './dom.js'
import randomColor from './randomColor.js'

const randColor = randomColor(0.375)

export const filmList = (parent, films) =>
	new CreateElement()
		.tag('div')
		.parent(parent)
		.options({ className: 'film__list' })
		.children(...films)
		.render()

export const filmItem = (episodId, title, crawl) =>
	new CreateElement()
		.tag('div')
		.css({ backgroundColor: randColor })
		.options({ className: 'film__item' })
		.children(
			new CreateElement()
				.tag('p')
				.options({ className: 'film-item__id', textContent: episodId }),
			new CreateElement()
				.tag('p')
				.options({ className: 'film-item__title', textContent: title }),
			new CreateElement()
				.tag('p')
				.options({ className: 'film-item__crawl', textContent: crawl }),
			new CreateElement()
				.tag('div')
				.options({ className: 'character__list-wrapper' })
				.children(
					new CreateElement().tag('h4').options({
						className: 'character__heading',
						textContent: 'Character',
					})
				)
		)
