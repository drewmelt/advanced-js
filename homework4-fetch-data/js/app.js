import { allStarWarsFilm, getCharacterInfo } from './fetch.js'
import { findElement, findAllElement } from './dom.js'
import { Loader } from './Loader.js'
import { characterList, characterItem } from './character.js'
import { filmList, filmItem } from './film.js'

const contentWrapper = findElement('.main__content-wrapper')

const showStarWarsList = async () => {
	const loader = new Loader(contentWrapper)
	loader.render()
	const starWarsSeries = await allStarWarsFilm()
	loader.remove()

	const films = starWarsSeries.map(film => {
		const { episodeId, openingCrawl, name } = film
		return filmItem(episodeId, name, openingCrawl)
	})

	filmList(contentWrapper, films)

	starWarsSeries.forEach((film, index) => {
		const currFilm = findAllElement('.character__list-wrapper')[index]
		const allRequsts = film.characters.map(getCharacterInfo)
		const loader = new Loader(currFilm)
		loader.render()

		Promise.all(allRequsts).then(data => {
			loader.remove()
			const people = data.map(person => characterItem(person.name))
			characterList(currFilm, people)
		})
	})
}

await showStarWarsList()
