import { CreateElement } from './dom.js'

export const characterList = (parent, people) =>
	new CreateElement()
		.tag('ul')
		.parent(parent)
		.options({ className: 'character__list' })
		.children(...people)
		.render()

export const characterItem = text =>
	new CreateElement()
		.tag('li')
		.options({ className: 'character__item', textContent: text })
