const StarWarsFilmSeriesUrl = 'https://ajax.test-danit.com/api/swapi/films'

export const getCharacterInfo = async url =>
	await fetch(url).then(res => res.json())

export const allStarWarsFilm = async () =>
	await fetch(StarWarsFilmSeriesUrl).then(res => res.json())
