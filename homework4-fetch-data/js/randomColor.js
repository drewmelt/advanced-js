const rand = max => Math.floor(Math.random() * max)

const randomcolor = opacity =>
	`rgba(${rand(255)},${rand(255)},${rand(255)},${opacity})`

export default randomcolor
