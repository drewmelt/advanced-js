import { insertElement } from './dom.js'

export class Loader {
	constructor(parent) {
		this.parent = parent
		this.elem = null
		this.loader =
			'<div class="lds-spinner"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>'
	}
	render() {
		this.elem = insertElement(this.parent, this.loader)
	}
	remove() {
		this.elem.remove()
	}
}
