const btn = document.querySelector('#getIp')
const content = document.querySelector('#content')

const getIp = async () => {
	const response = await fetch('https://api.ipify.org/?format=json')
	const { ip } = await response.json()
	return ip
}

const getUserData = async ip => {
	const response = await fetch(`http://ip-api.com/json/${ip}`)
	const data = await response.json()
	return data
}

const createUserCard = obj => {
	const createList = (obj, ...props) =>
		`<ul class="list">${props
			.map(prop =>
				obj[prop] ? `<li class="list__item">${obj[prop]}</li>` : ''
			)
			.join('')}</ul>`

	const list = createList(
		obj,
		'status',
		'continent',
		'country',
		'district',
		'regionName',
		'city'
	)
	return list
}

btn.addEventListener('click', async () => {
	const ip = await getIp()
	const data = await getUserData(ip)
	content.insertAdjacentHTML('beforeend', createUserCard(data))
})
