const books = [
	{
		author: 'Скотт Бэккер',
		name: 'Тьма, что приходит прежде',
		price: 70,
	},
	{
		author: 'Скотт Бэккер',
		name: 'Воин-пророк',
	},
	{
		name: 'Тысячекратная мысль',
		price: 70,
	},
	{
		author: 'Скотт Бэккер',
		name: 'Нечестивый Консульт',
		price: 70,
	},
	{
		author: 'Дарья Донцова',
		name: 'Детектив на диете',
		price: 40,
	},
	{
		author: 'Дарья Донцова',
		name: 'Дед Снегур и Морозочка',
	},
]

const translate = {
	name: 'Имя',
	author: 'Автор',
	price: 'Цена',
}

const validation = (obj, ...props) => {
	return props.reduce((acc, item) => {
		if (item in obj) {
			acc[item] = obj[item]
			return acc
		}
		throw new Error(`dont have prop ${item} in object`)
	}, {})
}

const insertToHTML = (data, place = document.body) => (
	place.insertAdjacentHTML('beforeend', data), place.lastElementChild
)

const createTag = (tag, className) => (...value) =>
	`<${tag} class="${className}">${value.join('')}</${tag}>`

const createList = arr => {
	const li = createTag('li', 'list__item')
	const ul = createTag('ul', 'list')
	return ul(
		arr
			.map(item => (Array.isArray(item) ? li(createList(item)) : li(item)))
			.join('')
	)
}

const filterListBook = arr => {
	const validationBook = obj => validation(obj, 'author', 'name', 'price')
	const validArr = arr.map(book => {
		try {
			return validationBook(book)
		} catch (e) {
			console.error(e, book)
		}
	})
	return validArr.filter(i => i)
}

const createBookList = arr => {
	const list = filterListBook(arr).map(item =>
		Object.keys(item).map(i => `${translate[i]}: ${item[i]}`)
	)
	return createList(list)
}

insertToHTML(createBookList(books), document.querySelector('#root'))

const deleteEmptyItem = arr => arr.filter(i => i)

const allClassName = parent => {
	const ans = []
	ans.push(parent.className)
	if (parent.children.length) {
		for (let i = 0; i < parent.children.length; i++) {
			ans.push(...allClassName(parent.children[i]))
		}
	}
	return deleteEmptyItem(ans)
}

// const allClassName = parent => {
// 	const ans = []
// 	ans.push(parent.className)
// 	if (parent.children.length)
// 		[...parent.children].forEach(child => ans.push(...allClassName(child)))

// 	return deleteEmptyItem(ans)
// }

const classNameStatistic = arr =>
	arr.reduce(
		(acc, item) => (item in acc ? acc[item]++ : (acc[item] = 1), acc),
		{}
	)

const generateUniqHash = () => {
	const start = 96
	const end = 122
	let counter = start
	let ans = ['a']
	return () => {
		if (counter === end) {
			counter = start
			ans = ['a', ...ans.map(i => 'a')]
		} else {
			ans[ans.length - 1] = String.fromCharCode(++counter)
		}
		return ans.join('')
	}
}
const renameClassToHash = () => {
	const hash = generateUniqHash()
	const arrKeys = Object.keys(classNameStatistic(allClassName(document.body)))
	return arrKeys.map(key => {
		const newName = hash()
		document
			.querySelectorAll('.' + key)
			.forEach(elem => (elem.className = newName))
		return { key, hash: newName }
	})
}

console.log(renameClassToHash())
